
/*
 * Skeleton function for Parallel Computing Class, 
 * Assignment: K-Means Algorithm (CUDA)
 *
 * To students: You should finish the implementation of k_means algorithm.
 *              You should add device functions/kernels to perform k_means on 
 *              GPU. The "k_means" function in this file is just an interface
 *              for passing in basic parameters needed.. You need to add GPU 
 *              kernels and launch them in the "k_means" function.
 *
 *              Note that the "k_means" function has two input parameters for
 *              block count and thread count per block. Please use these two
 *              parameters when launching your kernels.
 * 
 * Author:
 *     Wei Wang <wei.wang@utsa.edu>
 */
#include <stdio.h>
#include <float.h>
#include <math.h>

#include "k_means.h"


/*
 * k_means: k_means clustering algorithm implementation.
 *     struct point p[]: array of data points
 *     int m           : number of data points in p[]
 *     int k           : number of clusters to find
 *     int iters       : number of clustering iterations to run
 *     int block_cnt   : number of blocks to use
 *     int threads_per_block: number of threads per block
 *
 * Output parameters:   
 *     struct point u[]: array of cluster centers
 *     int c[]         : cluster id for each data points
 */

__global__ void do_k_means(struct point *p, 
	     int *m, 
	     int *k,
	     struct point *u,
	     int *c,
	     int *num_threads,
	     int *ppt)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int j = 0, h = 0;
	int currentMinCenterId = 0;
	double currentMinDist = 0, currentDist = 0;
	
	for(j = (index * (*ppt)); (j < ((index + 1) * (*ppt))) && (j < *m); j++){
		for(h = 0; h < *k; h++){
			currentDist = pow(p[j].x - u[h].x, 2) + pow(p[j].y - u[h].y, 2);
			if(h == 0){
				currentMinDist = currentDist;
				currentMinCenterId = h;
			}
			else{
				if(currentDist < currentMinDist){
					currentMinDist = currentDist;
					currentMinCenterId = h;
				}
			}
		}
		c[j] = currentMinCenterId;
	}
}

void k_means(struct point p[MAX_POINTS], 
	     int m, 
	     int k,
	     int iters,
	     struct point u[MAX_CENTERS],
	     int c[MAX_POINTS],
	     int block_cnt,
	     int threads_per_block)
{
	int j, i;
	int *g_m, *g_k, *g_c;
	struct point *g_p, *g_u;
	int m_size = sizeof(int), k_size = sizeof(int), c_size = m * sizeof(int), p_size = m * sizeof(struct point), u_size = k * sizeof(struct point);
	
	struct point sumCenters[MAX_CENTERS];
	int countPoints[MAX_CENTERS];
	
	int total_num_threads = block_cnt * threads_per_block;
	int points_per_thread = (int) ceil((double) m / (double) total_num_threads);
	int *num_threads, *ppt;
	int num_threads_size = sizeof(int), ppt_size = sizeof(int);
	
	/* randomly initialized the centers */
	for(j = 0; j < k; j++)
		u[j] = random_center(p);
	/*
	 * To students: This function is not a GPU kernel. You need to add your
	 * own kernel functions in this file. Block count and thread count per
	 * count are provided as parameters to this functions. Please use these
	 * two parameters when launching your kernels.
	 */
		
	cudaMalloc((void **) &g_m, m_size);
	cudaMalloc((void **) &g_k, k_size);
	cudaMalloc((void **) &g_p, p_size);
	cudaMalloc((void **) &g_u, u_size);
	cudaMalloc((void **) &g_c, c_size);
	cudaMalloc((void **) &num_threads, num_threads_size);
	cudaMalloc((void **) &ppt, ppt_size);
	
	cudaMemcpy(g_m, &m, m_size, cudaMemcpyHostToDevice);
	cudaMemcpy(g_k, &k, k_size, cudaMemcpyHostToDevice);
	cudaMemcpy(g_p, p, p_size, cudaMemcpyHostToDevice);
	cudaMemcpy(g_u, u, u_size, cudaMemcpyHostToDevice);
	cudaMemcpy(num_threads, &total_num_threads, num_threads_size, cudaMemcpyHostToDevice);
	cudaMemcpy(ppt, &points_per_thread, ppt_size, cudaMemcpyHostToDevice);
	cudaMemcpy(g_c, c, c_size, cudaMemcpyHostToDevice);
	
	for(j = 0; j < iters; j++){
		
		do_k_means<<<block_cnt,threads_per_block>>>(g_p, g_m, g_k, g_u, g_c, num_threads, ppt);
	
		cudaMemcpy(c, g_c, c_size, cudaMemcpyDeviceToHost);
	
		for(i = 0; i < k; i++){
			countPoints[i] = 0;
		}
			
		for(i = 0; i < m; i++){
			if(countPoints[c[i]] == 0){
				sumCenters[c[i]].x = p[i].x;
				sumCenters[c[i]].y = p[i].y;
				countPoints[c[i]] = 1;
			}
			else{
				sumCenters[c[i]].x += p[i].x;
				sumCenters[c[i]].y += p[i].y;
				countPoints[c[i]] += 1;
			}
		}

		for(i = 0; i < k; i++){
			if(countPoints[i] != 0){
				u[i].x = sumCenters[i].x / countPoints[i];
				u[i].y = sumCenters[i].y / countPoints[i];
			}
			else{
				u[i] = random_center(p);
			}
		}
	}
	
	cudaFree(g_m);
	cudaFree(g_k);
	cudaFree(g_p);
	cudaFree(g_u);
	cudaFree(num_threads);
	cudaFree(ppt);
	
  	return;
}
