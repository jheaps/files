/*
 * Skeleton function for Parallel Computing course, 
 * Assignment: K-Means Algorithm (MPI)
 *
 * To students: You should finish the implementation of k_means function
 * 
 * Author:
 *     Wei Wang <wei.wang@utsa.edu>
 */
#include <stdio.h>
#include <float.h>
#include <math.h>
#include <mpi.h>

#include "k_means.h"


/*
 * k_means: k_means clustering algorithm implementation.
 *
 * Input parameters:
 *     struct point p[]: array of data points
 *     int m           : number of data points in p[]
 *     int k           : number of clusters to find
 *     int iters       : number of clustering iterations to run
 *
 * Output parameters:   
 *     struct point u[]: array of cluster centers
 *     int c[]         : cluster id for each data points
 */
void k_means(struct point p[MAX_POINTS], 
	    int m, 
	    int k,
	    int iters,
	    struct point u[MAX_CENTERS],
	    int c[MAX_POINTS])
{
	/* TO STUDENTS: Please implement the MPI k_means here.
	 *	
	 * Note that, this time you will do the random initialization 
	 * yourself. The random function's interface is the same as 
	 * previous assignments. You can direct copy the old code here.
	 * However, you need to make sure that all your processes have
	 * the same initial centers (i.e., only initialized in one 
	 * process and send the initial centers to other processes).
	*/ 
	int j, i, h;
	int proc_id, num_procs, data_per_proc, points_to_process;
	double currentDist, currentMinDist, currentMinCenterId;
	int centers_to_process, center_start_index, base_centers_per_process;
	double newX, newY;
	int countPoints;
	MPI_Aint point_addr, x_addr, y_addr;
	int x_offset, y_offset;
	int blocklen[2] = {1,1};
	MPI_Datatype types[2] = {MPI_DOUBLE, MPI_DOUBLE};
	MPI_Datatype MPI_POINT;
	
	MPI_Comm_rank(MPI_COMM_WORLD, &proc_id);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	data_per_proc = (int) floor((double) m / (double) num_procs);
	base_centers_per_process = (int) floor ((double) k / (double) num_procs);
	
	if(proc_id == 0){	
		for(j = 0; j < k; j++){
			u[j] = random_center(p);
	
		}
	}
	
	MPI_Get_address(&(u[0]), &point_addr);
        MPI_Get_address(&(u[0].x), &x_addr);
        MPI_Get_address(&(u[0].y), &y_addr);
        x_offset = x_addr - point_addr;
        y_offset = y_addr - point_addr;
        MPI_Aint disp[2] = {x_offset, y_offset};
	MPI_Type_create_struct(2, blocklen, disp, types, &MPI_POINT);
        MPI_Type_commit(&MPI_POINT);
	
	if(proc_id == num_procs - 1){
		points_to_process = m - (proc_id * data_per_proc);
	}
	else{
		points_to_process = data_per_proc;
	}
	
	for(j = 0; j < iters; j++){
		
		if(proc_id == 0){
			for(i = 1; i < num_procs; i++){
				MPI_Send(u, k, MPI_POINT, i, i, MPI_COMM_WORLD);
			}
		}else{
			MPI_Recv(u, k, MPI_POINT, 0, proc_id, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
	
		for(i = proc_id * data_per_proc; (i < (proc_id * data_per_proc) + points_to_process) && (i < m); i++){
			
			for(h = 0; h < k; h++){
				currentDist = pow(p[i].x - u[h].x, 2) + pow(p[i].y - u[h].y, 2);
				
				if(h == 0){
					currentMinDist = currentDist;
					currentMinCenterId = h;
				}
				else{
					if(currentDist < currentMinDist){
						currentMinDist = currentDist;
						currentMinCenterId = h;
					}
				}
			}
			c[i] = currentMinCenterId;
		}
 		
		if(proc_id == 0){
			for(i = 1; i < num_procs; i++){
				if(i == num_procs - 1){
					MPI_Recv(&(c[i * data_per_proc]), m - (proc_id * data_per_proc), MPI_INT, i, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				}
				else{
					MPI_Recv(&(c[i * data_per_proc]), data_per_proc, MPI_INT, i, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
				}
			}
			
			
			for(i = 1; i < num_procs; i++){
				centers_to_process = base_centers_per_process;
				if(i < (k % num_procs)){
					centers_to_process++;
				}
				if(centers_to_process > 0){
					MPI_Send(c, k, MPI_INT, i, i, MPI_COMM_WORLD);
				}
				else{
					break;
				}
			}
			
			centers_to_process = base_centers_per_process;
	                if(proc_id < (k % num_procs)){
        	                centers_to_process++;
                	}

		}
		else{
                        MPI_Send(&(c[proc_id * data_per_proc]), points_to_process, MPI_INT, 0, proc_id, MPI_COMM_WORLD);
			centers_to_process = base_centers_per_process;
	                if(i < (k % num_procs)){
        	                centers_to_process++;
                	}

			if(centers_to_process > 0){
				MPI_Recv(c, k, MPI_INT, 0, proc_id, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			}
                }

			
		
		center_start_index = proc_id * centers_to_process;
		if((proc_id - (k % num_procs)) >= 0){
			center_start_index += k % num_procs;
		}
			
		for(h = center_start_index; (h < center_start_index + centers_to_process) && (h < k); h++){
			newX = 0;
			newY = 0;
			countPoints = 0;
			
			for(i = 0; i < m; i++){
				if(c[i] == h){
					newX += p[i].x;
					newY += p[i].y;
					countPoints++;
				}
			}
			
			if(countPoints > 0){
				u[h].x = newX / countPoints;
				u[h].y = newY / countPoints;
			}
			else{
				u[h] = random_center(p);
			}
		}
		
		if(proc_id == 0){
			for(i = 1; i < num_procs; i++){
                                centers_to_process = base_centers_per_process;
                                if(i < (k % num_procs)){
                                        centers_to_process++;
                                }
				
                                if(centers_to_process > 0){
					center_start_index = i * centers_to_process;
	                                if((i - (k % num_procs)) >= 0){
        	                                center_start_index += k % num_procs;
                	                }
					
                                        MPI_Recv(&(u[center_start_index]), centers_to_process, MPI_POINT, i, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                                }
                                else{
                                        break;
                                }
                        }
		}
		else{
			if(centers_to_process > 0){
				MPI_Send(&(u[center_start_index]), centers_to_process, MPI_POINT, 0, proc_id, MPI_COMM_WORLD);
			}
		}
		
	}
	
	return;
}
